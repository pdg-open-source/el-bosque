# El Bosque Wordpress Scaffold

![Super Sweet Logo](assets/el-bosque-logo.png)

El Bosque (The Forest) is a Wordpress scaffold build for use with [Lando](https://docs.devwithlando.io/), but works just as well if used in a different environment. With El Bosque you get the latest versions of the following:

* [Wordpress](https://wordpress.org/)
* [Timber](https://www.upstatement.com/timber/)
* [Lumber](https://gitlab.com/pdg-open-source/lumber) 

It's a simple but great place to start for green field projects.

## Requirements

If you are using Lando, that is all you need to have installed to get started. If you are using a different environment you will need to have the following:

* PHP 7.*
* Composer [Click here for instructions on installing](https://getcomposer.org/doc/00-intro.md)
* Basic knowledge of composer

## Getting Started

whether or not you are using Lando, getting started with El Bosque is super easy!

### Lando
Once Lando is installed, clone or download El Bosque, and rename to what ever you want. After that you will need to open the ```.lando.yml``` and change ```name: {{your site name}}`` to the name of your project (this will generate a pretty url for the site) then run the following commands:
```
cd path/to/your/project

lando start
 ```
And thats it! Lando will create an environment for you, and composer will install the latest version of WordPress, Timber, and the Lumber starter theme!

### No Lando

  If you are not using Lando the set up is just slightly different, because the composer.json is set up to install is set up to install all of the requirements into ``` /htdocs ``` so if your environment is not set up to look to ``` /htdocs ``` as a web root, you will need to edit the ```composer.json```  file as follows

```
{
    "name":"treighton/wp-starter",
    "repositories": [
        {           
            "type": "vcs",
            "url": "https://gitlab.com/pdg-open-source/starter-theme"
        },
        {
            "type": "composer",
            "url": "https://wpackagist.org"
        } 
    ],
    "extra": {
        "installer-paths": {
            "{{your webroot}}/wp-content/plugins/{$name}/": [
                "wpackagist-plugin/*",
                "type:wordpress-plugin"
            ],
            "{{your webroot}}/wp-content/themes/{$name}/":[
                "wpackagist-theme/*",
                "type:wordpress-theme"
            ]
        },
        "wordpress-install-dir": "{{your webroot}}"
    },
    "require": {
        "johnpbloch/wordpress": "4.*",
        "wpackagist-plugin/timber-library": "0.22.*",
        "treighton/lumber": "@dev"
    }
}
```
then save the file and run the following
```
cd path/to/your/project

composer install
 ```
Composer will install the latest version of WordPress, Timber, and the Lumber starter theme!

## Useful Links
* [Lando](https://docs.devwithlando.io/)
* [Composer](https://getcomposer.org/)
* [Timber](https://timber.github.io/docs/)
* [Lumber](https://github.com/treighton/Lumber/blob/master/README.md)
* [Using Composer with Wordpress](https://roots.io/using-composer-with-wordpress/)